from django.apps import AppConfig


class OpenIpamConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'apps.open_ipam'
